module.exports = function(grunt) {

  grunt.config('env', grunt.option('env') || process.env.ENV || 'prod');

  const config = require('./config/' + grunt.config('env') + '.json');

  const LANG = config._LANGUAGE;

  grunt.initConfig({
    'uglify': {
      options: {
        compress: grunt.config('env') === 'prod',
        beautify: grunt.config('env') === 'devel',
        mangle: false
      },
      my_target: {
        files: {
          'app/app.js': [
            'src/controllers/*.js',
            'src/services/*.js',
            'src/directives/*.js'
          ],
          'app/index.js': [
            'src/index.js'
          ],
          'app/lang.js': [
            'lang/'+LANG+'.js'
          ],
          'app/config/global.js': [
            'app/config/global.js'
          ]
        }
      }
    },
    copy: {
      config: {
        expand: true,
        cwd: 'config',
        src: 'global.js',
        dest: 'app/config',
        options: {
          process: function(content, srcpath) {
            return content.replace(
              '/*GRUNT_INCLUDE_ENVIRONMENT_VARS*/',
              JSON.stringify(config)
            );
          }
        }
      },
      angular: {
        expand: true,
        cwd: 'node_modules/angular',
        src: 'angular.min.js',
        dest: 'app/lib/',
      },
      angularRoute: {
        expand: true,
        cwd: 'node_modules/angular-route',
        src: 'angular-route.min.js',
        dest: 'app/lib/',
      },
      angularCookies: {
        expand: true,
        cwd: 'node_modules/angular-cookies',
        src: 'angular-cookies.min.js',
        dest: 'app/lib/',
      },
      bootstrap: {
        expand: true,
        cwd: 'node_modules/bootstrap/dist',
        src: ['fonts/*','js/bootstrap.js','css/bootstrap.min.css','css/bootstrap-theme.css'],
        dest: 'app/lib/bootstrap/',
      },
      sortable: {
        expand: true,
        cwd: 'node_modules/ng-sortable/dist',
        src: ['ng-sortable.min.js','ng-sortable.style.min.css'],
        dest: 'app/lib/',
      },
      templates: {
        expand: true,
        cwd: 'src',
        src: ['templates/*.html','templates/directives/*.html'],
        dest: 'app/'
      }
    },
    watch: {
      app: {
        files: [
            'src/controllers/*.js',
            'src/services/*.js',
            'src/directives/*.js',
            'src/config.js',
            'src/index.js',
            'lang/'+LANG+'.js',
            'src/templates/directives/*.html',
            'src/templates/*.html'
        ],
        tasks: ['uglify','copy:templates']
      }
    }
  });


  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-cssmin');

  grunt.registerTask('default', ['copy','uglify']);
  grunt.registerTask('sync', ['copy','uglify','watch']);

};