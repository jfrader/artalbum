<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class content extends CI_Controller {

	/*
	 * 
	 * ArtAlbum Content API Controller
	 *
	 * @author: Francisco Rader
	 * @email: franciscorader@gmail.com
	 * @project: ArtAlbum
	 * @git: https://gitlab.com/snead/artalbum.git
	 * @license: GPL
	 *  
	 */

	function __construct()
    {
        parent::__construct();
    }

    function albums()
    {
    	try {
    		$this->load->model('Album');
	    	$albums = $this->Album->getAlbumList();
	    	$this->output->set_content_type('text/json');
	    	echo json_encode($albums);
	    }
	    catch(Exception $e)
		{
			$this->output
			->set_status_header(415)
			->set_header('HTTP/1.1 415 NO DATA FOUND');
		}
    }

    function images($album = false) {
    	try {
    		$this->load->model('Album');
	    	$images = $this->Album->getImagesList($album);
	    	$this->output->set_content_type('text/json');
	    	echo json_encode($images);
	    }
	    catch(Exception $e)
		{
			$this->output
			->set_status_header(415)
			->set_header('HTTP/1.1 415 NO DATA FOUND');
		}
    }
}
