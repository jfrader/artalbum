/*
 * 
 * Artalbum App Angular Main Module
 *
 * @author: Francisco Rader
 * @email: franciscorader@gmail.com
 * @project: ArtAlbum
 * @git: https://gitlab.com/snead/artalbum.git
 * @license: GPL
 *  
 */
(function() {
    'use strict';

    angular
	    .module('app', ['ngRoute','ngCookies','as.sortable'])

	    .config(function($routeProvider, $httpProvider) {

	    	// ROUTE CONFIG
	        $routeProvider
	            // route for the home page
	            .when('/', {
	                templateUrl : 'app/templates/home.html',
	                controller  : 'homeController as vm'
	            })

	            // route for login
	            .when('/login', {
	            	templateUrl : 'app/templates/login.html',
	            	controller  : 'loginController as vm'
	            })

	            .when('/admin', {
	            	templateUrl : 'app/templates/admin.html',
	            	controller  : 'adminController as vm'
	            })

	            .when('/album/:albumUrl', {
	            	templateUrl : 'app/templates/album.html',
	            	controller 	: 'albumController as vm'
	            })

	            .when("/album/:albumUrl/:imageId", {
		            templateUrl: "app/templates/album.html",
		            controller: "albumController as vm"
		        });

	        // HTTP CONFIG
	        $httpProvider.defaults
	        	.headers.get = { 'Content-Type' : 'text/json' };
	        $httpProvider.defaults
	        	.headers.post = { 'Content-Type' : 'text/json' };
	    });
})();
