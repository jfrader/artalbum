/*
 * 
 * Album Directive for
 * ArtAlbum Angular app
 *
 * @author: Francisco Rader
 * @email: franciscorader@gmail.com
 * @project: ArtAlbum
 * @git: git
 * @license: GPL
 *  
 */
(function() {
	angular
	    .module('app')
	    .directive('albumList', albumList);

	function albumList() {

	    var directive = {
		    restrict: 'A',
		    templateUrl: '/app/templates/directives/album-list.html',
		    scope: {},
		    controller: AlbumListController,
		    controllerAs: 'vm',
	        bindToController: true, // because the scope is isolated
		    link: link,
		    compile: compile
	    };

	    return directive;

	    function compile(tElement, tAttrs, transclude) {
	        return {
	          pre: preLink,
	          post: postLink
	        }
	        function preLink(scope, element, attrs, controller) {
	        	scope.vm.getAlbums();

	     	}
	        function postLink(scope, element, attrs, controller) {

	        	$(element).bind("keydown keypress", function (event) {
		            if(event.which === 13) {
		                scope.$apply(function (){
		                    scope.$eval(attrs.myEnter);
		                });

		                event.preventDefault();		               
		                scope.vm.waiting = true;
		                scope.vm.saveAlbum({title: scope.vm.newAlbumTitle});		                
		            }
		        });		        
	        }
	    }

	    function link(scope, element, attrs, ctrl) {
	    	
	    }


		AlbumListController.$inject = ['$Albums','$scope','$Auth'];

		function AlbumListController($Albums, $scope, $Auth) {
			// Shared vm
			var vm = this;

			vm.albums = null;
			vm.waiting = true;
			vm.getAlbums = getAlbums;
			vm.saveAlbum = saveAlbum;
			vm.currentAlbum = currentAlbum;
			vm.newAlbumTitle = null;

			vm._ = {album: { newAlbum: _.album.newAlbum } };
			vm.isLogged = $Auth.checkLogged();


			function currentAlbum( id ) {
				if(id) {
					$Albums.highlight(id);
				}
				else {
					$Albums.highlight();
				}
			};

			function getAlbums(force) {
				this.force = force || false;
				vm.waiting = true;
				return $Albums.refresh(this.force).then(function(data){
					vm.albums = data;
					vm.waiting = false;
					return data;
				});
			};

			function saveAlbum(data, id) {				
				if(!data.title || data.title == null) {
					return _.album.emptyTitle;
				} else {
					vm.waiting = true;
					vm.albums = null;
					this.data = data;
					this.id = id || null;
					$Albums.saveAlbum(this.data, this.id).then(_success, _failure);

					function _success() {
						vm.getAlbums(true).then(function(data) {
							data[0].new = true;
							vm.newAlbumTitle = null;
						});
					};

					function _failure() {
						return _.album.newAlbumError;
						vm.waiting = false;
					};
				}
			};
		}
	}
})();