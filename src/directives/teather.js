/*
 * 
 * Teather Directive for
 * Angular ArtAlbum Application
 *
 * @author: Francisco Rader
 * @email: franciscorader@gmail.com
 * @project: ArtAlbum
 * @git: https://gitlab.com/snead/artalbum.git
 * @license: GPL
 *  
 */
 (function() {
 	angular
 	.module('app')
 	.directive('teather', teather);
 	
 	teather.$inject = [];
 	
 	function teather() {
 		
 		var directive = {
 			restrict: 'A',
 			templateUrl: 'app/templates/directives/teather.html',
 			replace: true,
 			scope: {
 				album: '=',
 				activeImage: '='
 			},
 			controller: teatherController,
 			controllerAs: 'vm',
 	        bindToController: true, // because the scope is isolated
 	        link: link,
 	        compile: compile
 	      };
 	      
 	      return directive;
 	      
 	      function compile(tElement, tAttrs, transclude) {
 	      	return {
 	      		pre: preLink,
 	      		post: postLink
 	      	}

 	      	function preLink(scope, iElement, iAttrs, controller) { 
 	      		
 	      	};
 	      	function postLink(scope, element, iAttrs, controller) {	

 	      		scope.vm.teatherElement = element;

 	      		$(element).find('.arrow').hide();

 	      		if (scope.vm.images.length > 0) {
 	      			$(element).modal();
 	      		}

 	      		$(element).find('.modal-dialog').on('mouseenter', function() {
 	      			arrowHideShow();
 	      		});

 	      		$(element).find('.modal-dialog').on('mouseleave', function() {
 	      			$(element).find('.arrow').hide();
 	      		});

 	      		$(element).find('.next').on('click', function() {
 	      			scope.vm.goNext(scope.vm.currentImage);
 	      			arrowHideShow();
 	      		});
 	      		$(element).find('.previous').on('click', function() {
 	      			scope.vm.goPrev(scope.vm.currentImage);
 	      			arrowHideShow();		 			
 	      		});

 	      		$(element).on('keydown', function( event ) {
 	      			if ( event.which == 39 && scope.vm.currentImage+1 < scope.vm.imageCount) {
 	      				scope.vm.goNext(scope.vm.currentImage);
 	      				arrowHideShow();
 	      			}
 	      		});
 	      		$(element).on('keydown', function( event ) {
 	      			if ( event.which == 37 && scope.vm.currentImage > 0) {
 	      				scope.vm.goPrev(scope.vm.currentImage);
 	      				arrowHideShow();
 	      			}
 	      		});

 	      		function arrowHideShow() {
 	      			$(element).find('.arrow').hide();
 	      			if (scope.vm.currentImage+1 < scope.vm.imageCount) {
 	      				$(element).find('.next').show();
 	      			}
 	      			if (scope.vm.currentImage > 0) {
 	      				$(element).find('.previous').show();
 	      			}
 	      		}
 	      		
 	      	};
 	      }
 	      
 	      function link(scope, element, attrs, ctrl) {
 	      	scope.vm.album = scope.album; 		
 	      }
 	      
 	    }
 	    
 	    teatherController.$inject = ['$scope','$location','$routeParams'];
 	    
 	    function teatherController($scope, $location, $routeParams) {
 		// Shared vm
 		var vm = this; 		
 		vm.images = vm.album.images || [];
 		vm.imageCount = vm.images.length;
 		vm.currentImage = $routeParams.imageId || 0;	

 		vm.setActive = function() {
 			angular.forEach($scope.vm.album.images, 
 				function(image, key){
 					image.active = false;
 					if(vm.currentImage == key) {
 						image.active = true;
 					}	
 				}	 			
 				);	  
 		} 

 		vm.setActive();

 		$scope.$parent.vm.openTeather = function (openOnKey) {
 			vm.currentImage = openOnKey || 0;
 			vm.setActive();			
 		};

 		vm.goPrev = function(current) {
 			$scope.$apply(function() {
 				vm.images[current].active = false;
 				vm.images[parseInt(current-1)].active = true;
 				vm.currentImage--;
 			});
 		};

 		vm.goNext = function(current) {
 			$scope.$apply(function() {
 				vm.images[current].active = false;
 				vm.images[parseInt(current+1)].active = true;
 				vm.currentImage++;
 			});
 			
 		};

 		$scope.$on("$destroy", function() {
 			$('.modal-backdrop').hide();
 			vm = null;
 		});
 	}
 })();