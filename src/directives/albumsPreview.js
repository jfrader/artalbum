/*
 * 
 * Albums preview
 *
 * @author: Francisco Rader
 * @email: franciscorader@gmail.com
 * @project: project
 * @git: git
 * @license: GPL
 *  
 */
 (function() {
 	angular
 	    .module('app')
 	    .directive('albumsPreview', albumsPreview);
 
 	albumsPreview.$inject = [];
 
 	function albumsPreview() {
 
 	    var directive = {
 		    restrict: 'A',
 		    templateUrl: 'app/templates/directives/albums-preview.html',
 		    scope: {},
 		    controller: albumsPreviewController,
 		    controllerAs: 'vm',
 	        bindToController: true
 	    };
 
 	    return directive;
 	}
 
 	albumsPreviewController.$inject = ['$Albums','$Images','$location'];
 
 	function albumsPreviewController($Albums, $Images, $location) {
 		// Shared vm
 		var vm = this;
 		vm.albums = [];

 		vm.goToAlbum = function( url ) {
 			var path = 'album/' + url;
 			console.log(path);
 			$location.path( path );
 		};

    vm.currentAlbum = function( id ) {
      if(id) {
        $Albums.highlight(id);
      }
      else {
        $Albums.highlight();
      }
    };


 		$Albums.refresh().then(function(data){
      vm.albums = $Albums.all; 			
 		});
 	}
 })();