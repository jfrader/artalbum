/*
 * 
 * Album Directive for
 * ArtAlbum Angular app
 *
 * @author: Francisco Rader
 * @email: franciscorader@gmail.com
 * @project: ArtAlbum
 * @git: git
 * @license: GPL
 *  
 */
(function() {
  angular
      .module('app')
      .directive('confirmation', confirmation);

  function confirmation() {

      var directive = {
        restrict: 'A',
        templateUrl: '/app/templates/directives/confirmation.html',
        scope: {
          label: '=',
          resolve: '=',
          reject: '=',
          key: '='
        },
        controller: confirmation,
        controllerAs: 'vm',
        bindToController: true,
        link: link,
        compile: compile
      };

      return directive;

      function compile(tElement, tAttrs, transclude) {
          return {
            pre: preLink,
            post: postLink
          }

          function preLink(scope, element, attrs, controller) {
          }
          function postLink(scope, element, attrs, controller) {        
          }
      }

      function link(scope, element, attrs, ctrl) {
        
      }


    confirmation.$inject = ['$Albums','$scope','$Auth'];

    function confirmation($Albums, $scope, $Auth) {
      // Shared vm
      var vm = this;

      vm.labels = _.admin.confirmation;
      vm.askedConfirmation = false;

      vm.ask = function() {
        vm.askedConfirmation = true;
      }

      vm.showLabel = function () {
        vm.askedConfirmation = false;
        if(vm.reject) { vm.reject(); }
      }

    };
  }
})();