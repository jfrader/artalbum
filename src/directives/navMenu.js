/*
 * 
 * Navigation Menu Directive
 * for ArtAlbum angular app
 *
 * @author: Francisco Rader
 * @email: franciscorader@gmail.com
 * @project: ArtALbum
 * @git: https://gitlab.com/snead/artalbum.git
 * @license: GPL
 *  
 */
 (function() {
 	angular
 	    .module('app')
 	    .directive('navMenu', ['$Albums', navMenu]);
 
 	function navMenu($Albums) {
 
 	    var directive = {
 		    restrict: 'A',
 		    templateUrl: 'app/templates/directives/nav-menu.html',
 		    scope: {
          currentAlbum: '='
        },
 		    controller: navMenuController,
 		    controllerAs: 'vm',
 	      bindToController: true, // because the scope is isolated
 		    link: link,
 		    compile: compile
 	    };
 
 	    return directive;
 
 	    function compile(tElement, tAttrs, transclude) {
 	        return {
 	          pre: preLink,
 	          post: postLink
 	        }
 	        function preLink(scope, iElement, iAttrs, ctrl) {
          }
 	        function postLink(scope, iElement, iAttrs, ctrl) {
 	        }
 	    }
 
 	    function link(scope, element, attrs, ctrl) {
      }
 
 	}
 
 	navMenuController.$inject = ['$scope','$Auth','myRouteParams', '$location', '$Albums'];
 
 	function navMenuController($scope, $Auth, myRouteParams, $location, $Albums) {
 		// Shared vm
 		var vm = this;

 		vm.basic	= _config.basic;
 		vm.links 	= _config.nav.links;
 		vm.labels 	= _;
 		vm.auth 	= $Auth;
 		vm.isLogged = $Auth.checkLogged();
 		vm.username = $Auth.getUsername();
 		vm.currentPath = myRouteParams.path;
 		vm.currentSubPath = myRouteParams.subpath;

    $scope.$watch(function() {
      return $Albums.current;
    }, function(newVal, oldVal) {
      vm.currentAlbum = newVal;
    }, true);

 		$scope.$on('$routeChangeStart', function(next, current) { 
 			vm.currentPath = myRouteParams.path;
      vm.currentSubPath = myRouteParams.subpath;
      $Albums.setCurrent(myRouteParams.subpath);
 		});
 	}
 })();