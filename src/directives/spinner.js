/*
 * 
 * Spinner directive for
 * Angular ArtAlbum Application
 *
 * @author: Francisco Rader
 * @email: franciscorader@gmail.com
 * @project: ArtAlbum
 * @git: https://gitlab.com/snead/artalbum.git
 * @license: GPL
 *  
 */
(function() {
	angular
	    .module('app')
	    .directive('spinner', spinner);

	spinner.$inject = [];

	function spinner() {

	    var directive = {
		    restrict: 'A',
		    templateUrl: 'app/templates/directives/spinner.html',
	    };

	    return directive;
	}
})();