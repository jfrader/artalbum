/*
 * 
 * Album Controller for
 * ArtAlbum app
 *
 * @author: Francisco Rader
 * @email: franciscorader@gmail.com
 * @project: ArtAlbum
 * @git: https://gitlab.com/snead/artalbum.git
 * @license: GPL
 *  
 */
 (function() {
    'use strict';
    
    angular
    .module('app')
    .controller('albumController', albumController);
    
    albumController.$inject = ['$Auth','$Albums','$Images','$routeParams', 'myRouteParams'];
    
    function albumController($Auth, $Albums, $Images, $routeParams, myRouteParams) {
        var vm = this;

        // Variables
        vm.albumUrl = $routeParams.albumUrl;
        vm.imageId = $routeParams.imageId; 
        vm.images = [];
        vm.loaded = false;
        vm.isLoading = true;
        vm.percentLoaded = 1;
        vm.isLogged = $Auth.checkLogged();
        vm.updating = false;
        vm.updateMessage = null;
        vm.labels = _.album;

        // Function declaration
        vm.getAlbum = getAlbum;
        vm.doUpdate = doUpdate;
        vm.doDeleteImage = doDeleteImage;

        init();
        function init() {
            getAlbum();
        }

        /*
         *  Drag and drop
         *
         */
         vm.sortableOptions = {
            orderChanged: function(event) {

                var destFile = event
                .dest
                .sortableScope
                .modelValue[event.dest.index];

                var srcFile = event
                .source
                .itemScope
                .sortableScope
                .modelValue[event.source.index];

                $Images.syncOrder(
                    destFile,
                    srcFile
                    ).then(function(res){
                        $Albums.refresh(true).then(function(albums) {                           
                            angular.forEach(albums, function(album){
                                if(album.title_url === vm.albumUrl)
                                {
                                    vm.album.images = album.images;
                                }
                            });
                        });
                    }).catch(function(err){
                        console.log(err);
                    });
                }
            };

            if (vm.isLogged === true) {
                vm.sortableOptions.containment = '.gallery';
            };

        /*
         *  Save Album
         *
         */
         function doUpdate () {

            vm.updating = true;

            _log(
                'trying to update: ',
                vm.album,
                vm.album.album_id
                );

            $Albums.saveAlbum( vm.album , vm.album.album_id ).then(_success, _failure);

            function _success () {
                vm.updating = false;
                vm.updateMessage = 1;
            };

            function _failure () {          
                vm.updating = false;    
                vm.updateMessage = vm.labels.form.editFailure;
            };
         }

        /*
         *  Delete Image from Album
         *
         */
         function doDeleteImage( id ) {
            $Images.deleteImage( id ).then(function(result){
                _log(result);
                $Albums.refresh(true).then(function(albums) {                           
                    angular.forEach(albums, function(album){
                        if(album.title_url === vm.albumUrl)
                        {
                            vm.album.images = album.images;
                        }
                    });
                });
            }).catch(function(result){
                _log(result);
            }
            );
         };

        /*
         *  Get Album
         *
         */
         function getAlbum () {
            $Albums.refresh().then(function(data){

                angular.forEach($Albums.all, function(album, key){
                    if(album.title_url === vm.albumUrl) {
                        vm.album = $Albums.all[key];
                        $Albums.setCurrent(myRouteParams.subpath);
                        $Albums.highlight(album.album_id);
                        var imageLocations = [];

                        angular.forEach(album.images, function(image){
                            imageLocations.push(
                                _config.mediaUrl.images+'/'+image.filename);
                        });

                        if(imageLocations.length > 0) {
                            $Images.download(imageLocations).then(
                                function handleResolve( imageLocations ) {                
                                    vm.isLoading = false;
                                    vm.loaded = true;                                   
                                    _log( "Images load was successful" );
                                },
                                function handleReject( imageLocation ) {
                                    vm.isLoading = false;
                                    vm.loaded = false;
                                    _log( "Images loading Failed", imageLocation );
                                    _log( "Images load Failure" );
                                },
                                function handleNotify( event ) {
                                    vm.percentLoaded = event.percent;
                                    _log( "Images percent loaded:", event.percent );
                                }
                                );
                        } else {
                            vm.loaded = true;
                            vm.isLoading = false;
                            vm.noImagesFound = true;
                        }           
                    }
                });
            });
         };
        }
    })();