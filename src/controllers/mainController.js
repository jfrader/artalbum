/*
 * 
 * Main controller for
 * artalbum angular app
 *
 * @author: Francisco Rader
 * @email: franciscorader@gmail.com
 * @project: ArtAlbum
 * @git: https://gitlab.com/snead/artalbum.git
 * @license: GPL
 *  
 */
 (function() {
    'use strict';

    angular
    	.module('app')
    	.controller('mainController', mainController);

    mainController.$inject = ['$location','$rootScope', '$Auth', '$Albums', 'myRouteParams'];

    function mainController($location, $rootScope, $Auth, $Albums, myRouteParams) {

    	/*
         *  Main Scope for
         *  child views and controllers
         *  and other variables
         */
    	    var _scope = this;

            // Scope vars
            _scope._    = _;            
            _scope.nav  = _config.nav; 
            _scope.isLogged = $Auth.checkLogged();
            _scope.currentAlbum = $Albums.current;

        /*
         *  On route change event
         */    
            $rootScope.$on('$routeChangeStart',
            function (event, next, current) {
               
               $Albums.setCurrent(myRouteParams.subpath);

                _scope.currentAlbum = $Albums.current;

                // Show admin nav link?
                _scope.isLogged = $Auth.checkLogged();
                if ( $location.url() === '/login'
                    || $location.url() === '/admin'
                    || _scope.isLogged) {                     
                    $Auth.showAdminNav = true;
                }

                // if route requires auth and user is not logged in
                if (!$Auth.hasAccess($location.url())) {
                    $location.path('/login');
                }                
            });
	}
 
 })();