/*
 * 
 * Login Controller for
 * ArtAlbum app
 *
 * @author: Francisco Rader
 * @email: franciscorader@gmail.com
 * @project: ArtAlbum
 * @git: https://gitlab.com/snead/artalbum.git
 * @license: GPL
 *  
 */
(function() {
    'use strict';

    angular
    	.module('app')
    	.controller('loginController', loginController);

    loginController.$inject = ['$Auth','$location'];

    function loginController($Auth, $location) {

    	// Scope
    	var vm = this;
    	vm.email 	= null;
    	vm.password = null;
    	vm.waiting  = false;
    	vm.message 	= null;
        vm.login = login;

    	function login() {

         	vm.waiting = true;                		
            $Auth.login(vm.email, vm.password).then(_success, _failure);

            function _success(response) {
                vm.isLogged = true;
            };

            function _failure(response) {
                _log(response.status);
                vm.waiting = false;
                if (response.status === 410) {
                    vm.message = _.login.badPassword;
                }
                if (response.status === 411) {
                    vm.message = _.login.userDoesntExist;
                }
            };
    	}
	}
})();