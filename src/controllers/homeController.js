/*
 * 
 * API Content Service
 * Art Album Angular App
 *
 * @author: Francisco Rader
 * @email: franciscorader@gmail.com
 * @project: ArtAlbum
 * @git: https://gitlab.com/snead/artalbum.git
 * @license: GPL
 *  
 */
 (function() {
     'use strict';
 
     angular
         .module('app')
         .controller('homeController', homeController);
 
     homeController.$inject = ['$scope','$interval'];
 
     function homeController($scope, $interval) {
     	/*
         * Scope declaration
         *
         */
         var vm = this;
         var _scope = $scope.$parent._scope;

        /*
         * Initialization
         *
         */

         init();
         function init() {

         }
         
         
     }
 })();