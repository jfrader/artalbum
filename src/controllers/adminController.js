/*
 * 
 * Admin Controller for
 * Art Album Angular App
 *
 * @author: Francisco Rader
 * @email: franciscorader@gmail.com
 * @project: ArtAlbum
 * @git: https://gitlab.com/snead/artalbum.git
 * @license: GPL
 *  
 */
 (function() {
     'use strict';
 
     angular
         .module('app')
         .controller('adminController', adminController);
 
     adminController.$inject = [];
 
     function adminController() {
         var vm = this;
     }
 })();