/*
 * 
 * Album Model and API Service
 * for ArtAlbum Angular App
 *
 * @author: Francisco Rader
 * @email: franciscorader@gmail.com
 * @project: ArtAlbum
 * @git: https://gitlab.com/snead/artalbum.git
 * @license: GPL
 *  
 */
 (function() {
    angular
  .module('app')
  .factory('$Albums', $Albums);

  $Albums.$inject = ['apiRequestService','$q','$interval','$Images','$cookies','myRouteParams'];

  function $Albums(apiRequestService, $q, $interval, $Images, $cookies, myRouteParams) {

     var service = {
        refresh: refresh,
        all: null,
        saveAlbum: saveAlbum,
        highlight: highlight,
        current: false,
        setCurrent: setCurrent
    };

    return service;

        function setCurrent(title_url) {
            angular.forEach(service.all, function(element, index) {
                if (element.title_url === title_url) {
                  service.current = element;
                }
            });
        }


        /*
         * Refresh albums (and images) from API
         *
          */
          function refresh( force ) {

            if(service.all === null || force) {

               var Request = apiRequestService.
               makeRequest(
                  _config.api.contentUrl + '/albums','GET');

               return Request.then(_success, _failure);

               } else {

                var deferred = $q.defer();
                deferred.resolve(service.all);
                return deferred.promise;
            }


        function _success(res) {
            _log(res.status, res.statusText);

            var albums = res.data;
            service.all = albums;
            _log(service.all, '----------------');

            return $Images.refresh(true).then(function(images){

                    // for each album
                    angular.forEach(albums, function(album, albumKey) {

                        // set images property of album
                        service.all[albumKey].images = [];

                        // for each image
                      angular.forEach(images, function(image) {

                            // put image into its album
                            if(image.parent_id === album.album_id) {
                                service.all[albumKey].images.push(image);
                            }
                        });
                      /* set cover image to the first */
                      service.all[albumKey].cover = service.all[albumKey].images[0];
                  });

                   return service.all;
               }, function(){
                   return service.all;
               });
        }

            function _failure(res) {
                //_log(res.status, res.statusText);

                return $q.reject(res.data);
            }   

        }; // End refresh()

        function deleteAlbum( id ) {
            return apiRequestService.makeRequest(
                _config.api.insertDataUrl + '/album',
                'DELETE',
                {
                    token: $cookies.getObject('sess').token,
                    album_id: id
                }
                );
        };

        function highlight( id ) { 
          angular.forEach(service.all, function(album){
            if(!id) {
              album.highlight = false;
          }
          if(album.album_id == id) {
              album.highlight = true;
          }  
      });
      };

        /*
         *  Save or update album
         *
          */
          function saveAlbum( data , id ) {

            var album = {};
            album.title   =  data.title || null;
            album.date    =  data.date  || null;
            album.text    =  data.text  || null;
            album.token   =  $cookies.getObject('sess').token;

            var deferred      =  $q.defer();
            var insertDataUrl = _config.api.insertDataUrl + '/album';

            if(id) {
                insertDataUrl = insertDataUrl + '/' + id;
            }

            if(album.title !== null) {
                var Request = apiRequestService.makeRequest(
                    insertDataUrl, 'POST', album 
                    );

                return Request.then(_success, _failure);

                function _success(res){
                    _log(res);
                    return res;
                };

                function _failure(res) {
                    deferred.resolve(res);
                    return deferred.promise;
                };

            } else {
                deferred.resolve(_.album.emptyTitle);
                return deferred.promise;
            }
        };

    }
})();