/*
 * 
 * API Requests Service
 * for ArtAlbum Angular App
 *
 * @author: Francisco Rader
 * @email: franciscorader@gmail.com
 * @project: ArtAlbum
 * @git: https://gitlab.com/snead/artalbum.git
 * @license: GPL
 *  
 */
(function() {
	angular
	    .module('app')
	    .factory('apiRequestService', apiRequestService);

	apiRequestService.$inject = ['$http'];

	function apiRequestService($http) {

		var service = {
			makeRequest: makeRequest,
			authenticate: authenticate,
			tokenAuth: tokenAuth,
			deAuthenticate: deAuthenticate,
			transformRequest: false
		};

		return service;

		function makeRequest(url, method, data, headers, transformRequest) {

			var request = {
				method: 	method 		|| 'GET',
				url: 		url 		||  $config.API_URL,
				data: 		data 		||  {},
				headers: 	headers 	||  null
			};

			if(service.transformRequest) {
				request.transformRequest = service.transformRequest;
			}

			console.log(request);
			return $http(request);
		};

		function authenticate(email, password) {

			var url 	= _config.api.authUrl + '/auth';
			var data 	= {
				email: email,
				password: password
			};

			return service.makeRequest(url, 'POST', data);
		}

		function tokenAuth(token) {

			var url = _config.api.authUrl + '/tokenAuth/';

			return service.makeRequest(url, 'POST', {token: token});
		}

		function deAuthenticate() {

			var url = _config.api.authUrl + '/logout/';

			return service.makeRequest(url, 'GET');
		}
	}
})();